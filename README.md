Swapfile
========

This role's duty is to create and configure a swap file.

Variables
---------

### `swapfile_path`
The path to the swap file. By default, this is `/swapfile`.

### `swapfile_size`
The size of the swap file. By default, this is `4G`.

### `swapfile_swappiness`
The system swappiness. By default, this is `1`.

Example
-------

```yaml
---
swapfile_path: /swapfile
swapfile_size: 4G
swapfile_swappiness: 1
```
